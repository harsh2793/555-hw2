package edu.upenn.cis.cis455;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class TestStorage {

    String documentContents = "Testing Contents to check if the database gets the correct values";
    String documentContents1 = "Second Testing Contents to check if the database gets the correct values";
    String db = "./database1";
    StorageInterface database = null;

    static void deleteFolder(File file) {
        for (File subFile : file.listFiles()) {
            if (subFile.isDirectory()) {
                deleteFolder(subFile);
            } else {
                subFile.delete();
            }
        }
        file.delete();
    }

    @Before
    public void setUp() {

        if (!Files.exists(Paths.get(db))) {

            try {
                Files.createDirectory(Paths.get(db));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        database = StorageFactory.getDatabaseInstance(db);
    }

    // 1. First Test Case for DataBase.
    @Test
    public void testDocumentDatabase() throws IOException {

        String testUrl = "http://localhost/testing.html";
        String testUrl2 = "http://localhost/testing1.html";
        // Generate Date
        Date date = new Date();
        SimpleDateFormat getDate = new SimpleDateFormat("EEE, dd-MMM-yy HH:mm:ss z");
        String dateFormat = getDate.format(date);

        // Testing Add document if not exists.
        int id = database.addDocument(testUrl, documentContents, dateFormat);
        System.out.println("ID:" + id);
        assertTrue(id != -1);

        int id2 = database.addDocument(testUrl2, documentContents1, dateFormat);
        assertTrue(id != -1);

        // Testing get document.
        String getDoc = database.getDocument(testUrl);
        assertTrue(getDoc.equals(documentContents));

        // Testing get document if document exists.
        id = database.addDocument(testUrl, documentContents, null);
        assertTrue(id == -1);

        // Get date from url.
        String getDateFromDb = database.getDateUrl(testUrl);
        assertTrue(getDateFromDb.equals(dateFormat));

        // Check if contains URL.
        Boolean ret = database.containsUrl(testUrl);
        assertTrue(ret);

        // Check the corpus size.
        int corp = database.getCorpusSize();
        assertTrue(corp == 2);

    }

    // 2. Second Test Case for User Database.
    @Test
    public void testUserDatabase() throws IOException {

        // Get session for a not registered user.
        Boolean ret = database.getSessionForUser("newUser", "password");
        assertTrue(ret == false);

        // Register new user.
        int uid = database.addUser("newUser", "password");
        assertTrue(uid != -1);

        // Get session for valid user.
        ret = database.getSessionForUser("newUser", "password");
        assertTrue(ret == true);

        // Try to Register already existing user.
        uid = database.addUser("newUser", "password");
        assertTrue(uid == -1);

    }

    @After
    public void tearDown() {
        database.close();
        deleteFolder(new File(db));
    }
}
