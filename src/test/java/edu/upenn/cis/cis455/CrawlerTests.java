package edu.upenn.cis.cis455;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.crawler.CrawlerWorkers;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.RobotDataObj;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class CrawlerTests {

    String message = "Inside testPrintMessage()";
    CrawlerWorkers crawl = null;

    @Before
    public void setUp() {

        crawl = new CrawlerWorkers();
    }

    @Test
    public void crawlerRobotTest1() throws IOException {

        String robourl = "http://crawltest.cis.upenn.edu:80/robots.txt";
        String url1 = "http://crawltest.cis.upenn.edu/marie/private";
        String url2 = "http://crawltest.cis.upenn.edu/marie/private/middleeast.xml";

        URLInfo info = new URLInfo(url1);
        URL urlVal = new URL(info.toString());
        HttpURLConnection connection = (HttpURLConnection) urlVal.openConnection();

        URL robotURL = new URL(robourl);
        String retString = crawl.getContent(robotURL, null, connection, true);
        RobotDataObj obj = crawl.parseRobots(retString);
        boolean ret = crawl.checkIfNotDisallowed(info, obj);

        // Check if the returned val is true as the link is allowed.
        assert (ret == true);

        info = new URLInfo(url2);
        urlVal = new URL(info.toString());
        connection = (HttpURLConnection) urlVal.openConnection();

        retString = crawl.getContent(robotURL, null, connection, true);
        obj = crawl.parseRobots(retString);
        ret = crawl.checkIfNotDisallowed(info, obj);

        // Check if the returned val is true as the link is disallowed.
        assertTrue(ret == false);
    }

    @Test
    public void crawlerTestCheckHeaders2() throws IOException {

        Map<String, List<String>> headerVals = new HashMap<String, List<String>>();

        List<String> newList = new ArrayList<String>();
        newList.add("img/png");
        headerVals.put("Content-Type", newList);
        newList.clear();
        newList.add("12");
        headerVals.put("Content-Length", newList);

        // Incorrect Headers.
        boolean ret = crawl.checkHeaders(headerVals);
        assertTrue(ret == false);

        newList.clear();
        newList.add("img/png");
        headerVals.put("Content-Type", newList);
        newList.clear();
        newList.add("0");

        // No content Length and type.
        ret = crawl.checkHeaders(headerVals);
        assertTrue(ret == false);
    }

    @Test
    public void crawlerTestGetContent3() throws IOException {

        // Get the content of the autograder test website and check if it is correct.
        String url = "https://mike2151.github.io/test-crawler-server/";

        URLInfo info = new URLInfo(url);
        URL urlVal = new URL(info.toString());
        HttpsURLConnection connection = (HttpsURLConnection) urlVal.openConnection();

        String cont = crawl.getContent(urlVal, connection, null, false);

        // Send a get request and check if it is correct.
        assert (cont.startsWith("<HTML><HEAD><TITLE>CIS555 HW2MS1 Grading</TITLE></HEAD><BODY>"));
    }

    @Test
    public void crawlerTestGetHeaders4() throws IOException {

        // Send a head request to get the headers of the autograder test website.
        String url = "https://mike2151.github.io/test-crawler-server/";

        URLInfo info = new URLInfo(url);
        URL urlVal = new URL(info.toString());
        HttpsURLConnection connection = (HttpsURLConnection) urlVal.openConnection();

        Map<String, List<String>> headers = crawl.sendHeadReq(connection, null, false, null);

        for (String newStr : headers.keySet()) {
            for (String s : headers.get(newStr)) {
                System.out.println(newStr + ":" + s);
            }
        }

        // Check the content-type and content-length.
        assert (headers.get("Content-Type").toString().contains("text/html"));
        assert (headers.get("Content-Length").toString().contains("262"));
    }

    @After
    public void tearDown() {
    }
}
