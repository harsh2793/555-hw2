package edu.upenn.cis.cis455.crawler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import edu.upenn.cis.cis455.storage.RobotDataObj;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;

/**
 * Class for the crawler master.
 */
public class Crawler implements CrawlMaster {

    // Build a thread pool of CrawlerWorkers.
    static final int NUM_WORKERS = 10;

    // Variables for the input.
    String startUrl;
    StorageInterface db;
    int size;
    int count;

    // Data structure for the crawler.
    ArrayList<CrawlerWorkers> crawlerThreadPool = new ArrayList<CrawlerWorkers>();
    ArrayList<String> urls = new ArrayList<String>();
    AtomicInteger docCount = new AtomicInteger(0);
    Map<String, RobotDataObj> robotMapCrawl = new HashMap<String, RobotDataObj>();
    Map<String, String> timeLast = new HashMap<String, String>();
    Set<String> urlCrawledSet = new HashSet<String>();
    CrawlerThreadPool threadPool = new CrawlerThreadPool();
    int countExit = 0;
    int countWorking = 0;

    /**
     * Constructor for the crawler.
     * 
     * @param startUrl
     * @param db
     * @param size
     * @param count
     */
    public Crawler(String startUrl, StorageInterface db, int size, int count) {

        // Initialize the variables.
        this.startUrl = startUrl;
        this.db = db;
        this.size = size;
        this.count = count;
        this.docCount.set(0);
    }

    /**
     * Main thread
     */
    public void start() {

        // Add the start URL to the thread queue.
        urls.add(startUrl);
        threadPool.addQueue(urls);

        // Create thread pool.
        for (int i = 0; i < NUM_WORKERS; i++) {

            // Start worker how we did threadpool.
            CrawlerWorkers newWorker = new CrawlerWorkers(urls, db, size, this, threadPool, robotMapCrawl,
                    urlCrawledSet, timeLast);
            crawlerThreadPool.add(newWorker);
            newWorker.start();
        }
    }

    /**
     * Stop the threads by notifying them and bringing them out of the wait.
     */
    public void stop() {

        threadPool.stopQueue();
    }

    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {

        // Increment the count.
        docCount.incrementAndGet();
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {

        // Check if the workers should exit.
        if ((docCount.get() >= count) || ((countWorking == 0) && (urls.size() == 0))) {
            stop();
            return true;
        }

        return false;
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {

        if (working) {
            countWorking++;
        } else {
            countWorking--;
        }
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {

        countExit++;

        if (countExit == NUM_WORKERS) {
            System.exit(0);
        }
    }

    /**
     * Final Shutdown
     */
    public void finalShutDown() {

        while (countExit < NUM_WORKERS) {

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) {
        if (args.length < 3 || args.length > 5) {
            System.out.println(
                    "Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);

        Crawler crawler = new Crawler(startUrl, db, size, count);

        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();

        while (!crawler.isDone())
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                // e.printStackTrace();
            }

        // Final shutdown
        crawler.finalShutDown();

        System.out.println("Done crawling!");
    }
}
