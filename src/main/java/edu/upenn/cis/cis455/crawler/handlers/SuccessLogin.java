package edu.upenn.cis.cis455.crawler.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Route;

public class SuccessLogin implements Route {
    Logger logger = LogManager.getLogger(LoginFilter.class);

    public SuccessLogin() {
    }

    @Override
    public Object handle(Request req, Response response) throws Exception {

        // Check for session and redirect to login if does not exist.
        if (req.session(false) == null) {
            logger.info("Not logged in - redirecting!");
            response.redirect("/login-form.html", 302);
        } else {

            // Return the success page with username as mentioned in handout.
            String body = "<!DOCTYPE html><html><head><title>Successful Login</title></h1></head><body>Successful Login into the HW2 for user: "
                    + req.session().attribute("user")
                    + " </body></br><h2><a href=\"/logout\">Logout User</h2></html>\r\n";
            return body;
        }

        return response;
    }
}
