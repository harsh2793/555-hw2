package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Response;
import spark.Route;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class LookUpHandler implements Route {

    Logger logger = LogManager.getLogger(LogoutUser.class);
    final StorageInterface db;

    // Constructor for lookup.
    public LookUpHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public Object handle(Request req, Response response) throws Exception {

        // Type of response.
        response.type("text/html");

        URLInfo docUrl = new URLInfo(req.queryParams("url"));

        // Get the document.
        String doc = db.getDocument(docUrl.toString());
        if (doc == null) {
            response.status(404);
        } else {
            response.status(200);
        }

        
        return doc;
    }

}
