package edu.upenn.cis.cis455.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import com.sleepycat.je.ThreadInterruptedException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.RobotDataObj;
import edu.upenn.cis.cis455.storage.StorageInterface;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.client.HttpConnection;
import org.eclipse.jetty.client.HttpContentResponse;

/**
 * Class for the workers of the crawler
 */
public class CrawlerWorkers extends Thread {

    // Data structures for workers.
    ArrayList<String> shareQueue;
    StorageInterface db;
    int size;
    CrawlMaster crawlerObj;
    CrawlerThreadPool threadPool = null;
    Map<String, RobotDataObj> robotMapCrawl;
    Set<String> urlCrawledSet = new HashSet<String>();
    Map<String, String> timeLast;
    Logger logger = LogManager.getLogger(CrawlerWorkers.class);

    // Empty constructor for testing.
    public CrawlerWorkers() {
    }

    /**
     * @param shareQueue
     * @param db
     * @param size
     * @param crawlerObj
     * @param threadPool
     * @param disallow
     * @param timeDelay
     */
    // Constructor or the crawler.
    public CrawlerWorkers(ArrayList<String> shareQueue, StorageInterface db, int size, CrawlMaster crawlerObj,
            CrawlerThreadPool threadPool, Map<String, RobotDataObj> robotMapCrawl, Set<String> urlCrawledSet,
            Map<String, String> timeLast) {

        this.shareQueue = shareQueue;
        this.db = db;
        this.size = size;
        this.crawlerObj = crawlerObj;
        this.threadPool = threadPool;
        this.robotMapCrawl = robotMapCrawl;
        this.urlCrawledSet = urlCrawledSet;
        this.timeLast = timeLast;
    }

    // Check if it exists in the crawled map for this run
    public Boolean checkIfCrawledNow(String url) {

        if (urlCrawledSet.contains(url)) {
            return true;
        }

        return false;
    }

    // Check if the headers are valid.
    public boolean checkHeaders(Map<String, List<String>> headerVals) {

        int contentLength = Integer.parseInt(headerVals.get("Content-Length").get(0));

        if ((contentLength > 0) && contentLength <= ((this.size) * 1000000)
                && (headerVals.containsKey("Content-Type")
                        && (headerVals.get("Content-Type").toString().contains("text/html")
                                || headerVals.get("Content-Type").toString().contains("text/xml")
                                || headerVals.get("Content-Type").toString().contains("application/xml")
                                || headerVals.get("Content-Type").toString().contains("+xml")))) {
            return true;
        }

        return false;
    }

    // Send the head request and get the headers map.
    public Map<String, List<String>> sendHeadReq(HttpsURLConnection connection, HttpURLConnection connectionS,
            boolean checkifmod, String checkDate) throws IOException {

        Map<String, List<String>> headerVals = new HashMap<String, List<String>>();
        int responseCode = -1;

        // Get the header values.
        if (connection == null) {

            connectionS.setRequestMethod("HEAD");
            connectionS.setRequestProperty("User-Agent", "cis455crawler");
            if (checkifmod) {
                connectionS.setRequestProperty("If-Modified-Since", checkDate);
            }
            connectionS.connect();
            headerVals = connectionS.getHeaderFields();
            responseCode = connectionS.getResponseCode();
            connectionS.disconnect();
        } else {

            connection.setRequestMethod("HEAD");
            connection.setRequestProperty("User-Agent", "cis455crawler");
            if (checkifmod) {
                connection.setRequestProperty("If-Modified-Since", checkDate);
            }
            connection.connect();
            headerVals = connection.getHeaderFields();
            responseCode = connection.getResponseCode();
            connection.disconnect();
        }

        System.out.println("Status Code" + responseCode);

        // Check if the response code was not modified.
        if (responseCode == HttpURLConnection.HTTP_NOT_MODIFIED) {

            return null;
        }

        if ((responseCode == HttpURLConnection.HTTP_MOVED_PERM)
                || (responseCode == HttpURLConnection.HTTP_MOVED_TEMP)) {

            String locRedirect = headerVals.get("Location").toString();
            System.out.println("Redirecting to: " + locRedirect);
            threadPool.urlEnqueue(locRedirect);
            return null;
        }

        if (responseCode != HttpURLConnection.HTTP_OK) {
            return null;
        }

        return headerVals;

    }

    // Add the URLs on the page using Jsoup.
    public void addURLsToQueue(URLInfo url) throws IOException {

        Document docret = Jsoup.connect(url.toString()).get();
        Elements hrefs = docret.select("a[href]");

        System.out.println("Links:" + hrefs.size());
        for (Element link : hrefs) {
            System.out.println(link.attr("abs:href"));
            if (link.attr("abs:href").startsWith("http")) {

                threadPool.urlEnqueue(link.attr("abs:href"));
            } else {

                threadPool.urlEnqueue((url.isSecure() ? "https://" : "http://") + url.getHostName() + ":"
                        + url.getPortNo() + link.attr("abs:href"));
            }
        }
    }

    // Get the content of the URL.
    public String getContent(URL urlVal, HttpsURLConnection connection, HttpURLConnection connectionS, boolean robot)
            throws IOException {

        BufferedReader buffer;
        String documentContents = "";

        if (connection == null) {

            connectionS = (HttpURLConnection) urlVal.openConnection();
            connectionS.setRequestMethod("GET");
            connectionS.setReadTimeout(50000);
            connectionS.connect();

            if (connectionS.getResponseCode() == 404) {

                connectionS.disconnect();
                return null;
            }
            buffer = new BufferedReader(new InputStreamReader(connectionS.getInputStream()));

            String line = null;
            while ((line = buffer.readLine()) != null) {
                if (robot) {
                    documentContents += (line + "\n");
                } else {
                    documentContents += (line + "\r\n");
                }
            }

            if (!robot) {

                String finalStr = documentContents.substring(0, documentContents.length() - 2);
                documentContents = finalStr;
            }

            connectionS.disconnect();

        } else {

            connection = (HttpsURLConnection) urlVal.openConnection();
            connection.setRequestMethod("GET");
            connection.setReadTimeout(50000);
            connection.connect();

            if (connection.getResponseCode() == 404) {

                connection.disconnect();
                return null;
            }

            buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line = null;
            while ((line = buffer.readLine()) != null) {
                if (robot) {
                    documentContents += (line + "\n");
                } else {
                    documentContents += (line + "\r\n");
                }
            }

            if (!robot) {

                String finalStr = documentContents.substring(0, documentContents.length() - 2);
                documentContents = finalStr;
            }

            connection.disconnect();
        }

        return documentContents;

    }

    // Check if there is a delay and move it to the back of the queue.
    public Boolean waitCrawl(String url) {

        return false;
    }

    // Check the map if the content was seen previously:
    // Zach said -> DocID->MD5(content)
    public Boolean contentSeen(String url) {

        return this.db.containsUrl(url);
    }

    // Get the crawlDelay and delay for the mentioned time.
    public void crawlDelay(URLInfo url, int delay) {
        long sleepTime = -1;

        if (timeLast != null && timeLast.containsKey(url.toString())) {

            // Current time.
            // d1 - d2 -> time in seconds
            // delay - time in second (<0)
            Date date = new Date();
            SimpleDateFormat getDate = new SimpleDateFormat("EEE, dd-MMM-yy HH:mm:ss z");
            String dateFormat = getDate.format(date);

            Date date1 = null;
            try {
                date1 = getDate.parse(timeLast.get(url.toString()));
                date = getDate.parse(dateFormat);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            long diff = date.getTime() - date1.getTime();
            sleepTime = delay - diff;

        } else {

            sleepTime = delay;
        }

        if (sleepTime < 0) {

            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
            }
        }
    }

    // Check if the URL is disallowed in the robots.txt
    public boolean checkIfNotDisallowed(URLInfo url, RobotDataObj robo) {

        List<String> disallowed = new ArrayList<>();
        int delay = -1;

        if (robo.getDisallowedLinks("cis455crawler") != null) {

            disallowed = robo.getDisallowedLinks("cis455crawler");
            delay = robo.getCrawlDelay("cis455crawler");

        } else if (robo.getDisallowedLinks("*") != null) {

            disallowed = robo.getDisallowedLinks("*");
            delay = robo.getCrawlDelay("*");
        }

        if (disallowed != null) {

            for (String i : disallowed) {
                System.out.println(i);
            }
        }

        if (disallowed.size() != 0) {

            String path = url.getFilePath();

            for (String i : disallowed) {
                if (path.startsWith(i)) {
                    System.out.println("path disallowed: " + path + " : " + url.toString());
                    return false;
                }
            }

            if (disallowed.contains(path)) {
                System.out.println("path disallowed: " + path + " : " + url.toString());
                return false;
            }

            if (delay != -1) {
                crawlDelay(url, delay);
            }
        }

        return true;
    }

    // Parser for the robots.txt
    public RobotDataObj parseRobots(String robotsFile) {

        String[] split = robotsFile.split("\n");
        RobotDataObj newObj = new RobotDataObj();

        for (int i = 0; i < split.length; i++) {

            if (split[i].contains("#")) {
                continue;
            }

            String useragent = "";
            List<String> disallowed = new ArrayList<String>();
            String delay = "";

            if (split[i].contains("User-agent:")) {
                useragent = split[i].substring(12);
                i++;
            }

            while ((i < split.length) && (split[i].length() != 0)) {
                if (split[i].contains("Disallow:")) {
                    disallowed.add(split[i].substring(10));
                } else if (split[i].contains("Crawl-delay:")) {
                    delay = split[i].substring(13);
                }

                i++;
            }

            newObj.addEntry(useragent, disallowed, (delay.length() != 0) ? Integer.parseInt(delay) : -1);
        }

        return newObj;
    }

    // Verify that it can be crawled by parsing robots.txt
    public Boolean checkRobots(URLInfo info, HttpsURLConnection connection, HttpURLConnection connectionS) {

        String urlPath = info.getHostName();

        String url = (info.isSecure() ? "https://" : "http://") + info.getHostName() + ":" + info.getPortNo()
                + "/robots.txt";
        System.out.println("Checking robot of: " + url);

        // Check if it exists in Database.
        String robotRet = db.checkRobotExists(urlPath);
        if (robotRet != null) {

            return checkIfNotDisallowed(info, db.getRobots(urlPath));

        } else {
            String retString = "";
            URL robotURL;
            try {
                robotURL = new URL(url);
                if (info.isSecure()) {

                    retString = getContent(robotURL, connection, null, true);
                } else {
                    retString = getContent(robotURL, null, connectionS, true);
                }

                if (retString == null) {
                    return true;
                }
                RobotDataObj newObj = parseRobots(retString);

                db.addRobots(urlPath, newObj);

                // Add it to the map to check next time for crawled robot.txt
                robotMapCrawl.put(urlPath, newObj);

                return checkIfNotDisallowed(info, newObj);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        return true;
    }

    // Check if the hostname has robot file in the map.
    public RobotDataObj checkRobotsExists(URLInfo info) {

        if (robotMapCrawl.containsKey(info.getHostName())) {

            return robotMapCrawl.get(info.getHostName());
        }

        return null;
    }

    // Run the crawler.
    public void run() {

        while (!crawlerObj.isDone()) {

            String url = null;

            try {
                url = threadPool.urlDequeue();

                if (url == null) {
                    continue;
                }

                System.out.println("Parsing the url:" + url);
                crawlerObj.setWorking(true);
                crawlerHelp(url);
                crawlerObj.setWorking(false);

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                System.out.println("Interrupted Exception");
            }
        }

        crawlerObj.notifyThreadExited();
    }

    // Helper function for crawling and checking the conditions.
    public void crawlerHelp(String url) {

    	System.out.println("URL" + url);
        URLInfo info = new URLInfo(url);
        URL urlVal;
        RobotDataObj robo = null;

        try {

        	System.out.println("Info:" + info.toString());
            urlVal = new URL(info.toString());

            // Check if it was crawled in this session.
            if (!checkIfCrawledNow(url)) {

                // Check if robot.txt is parsed for the hostname in this crawl.
                robo = checkRobotsExists(info);

                // Create connections based on HTTP and HTTPS.
                if (info.isSecure()) {

                    HttpsURLConnection connection = (HttpsURLConnection) urlVal.openConnection();

                    // This will check if I am allowed to crawl the file.
                    if (robo == null) {

                        if (!checkRobots(info, connection, null)) {
                            logger.info(url + ": disallowed");
                            return;
                        }

                    } else {

                        if (!checkIfNotDisallowed(info, robo)) {
                            logger.info(url + ": disallowed");
                            return;
                        }
                    }

                    System.out.println("Robot Crossed");

                    Map<String, List<String>> headers = new HashMap<String, List<String>>();
                    // Check if the url was seen before
                    if (contentSeen(info.toString())) {

                        String lastChecked = this.db.getDateUrl(info.toString());
                        System.out.println("LastChecked:" + lastChecked);
                        headers = sendHeadReq(connection, null, true, lastChecked);

                        if (headers == null) {
                            logger.info(url + ": not modified");
                            return;
                        }

                        if (!checkHeaders(headers)) {
                            logger.info(url + ": incorrect headers");
                            return;
                        }

                    } else {

                        headers = sendHeadReq(connection, null, true, "");

                        if (headers == null) {
                            logger.info(url + ": not modified");
                            return;
                        }

                        if (!checkHeaders(headers)) {

                            logger.info(url + ": incorrect headers");
                            return;
                        }
                    }

                    if (waitCrawl(url)) {
                    }

                    String documentContents = getContent(urlVal, connection, null, false);
                    crawlerObj.incCount();
                    addURLsToQueue(info);

                    Date date = new Date();
                    SimpleDateFormat getDate = new SimpleDateFormat("EEE, dd-MMM-yy HH:mm:ss z");
                    this.db.addDocument(info.toString(), documentContents, getDate.format(date));
                    logger.info(url + ": downloading");
                    System.out.println(url + ": downloading");
                    System.out.println("Content");
                    System.out.println(documentContents);

                    // Add it to the map to check next time.
                    urlCrawledSet.add(url);

                    timeLast.put(url, getDate.format(date));

                } else {

                    HttpURLConnection connection = (HttpURLConnection) urlVal.openConnection();

                    // This will check if I am allowed to crawl the file.
                    if (robo == null) {

                        if (!checkRobots(info, null, connection)) {

                            logger.info(url + ": disallowed");
                            System.out.println(url + " -> Not allowed by robot");
                            return;
                        }

                    } else {

                        if (!checkIfNotDisallowed(info, robo)) {
                            logger.info(url + ": disallowed");
                            System.out.println(url + " -> Not allowed by robot");
                            return;
                        }
                    }

                    Map<String, List<String>> headers = new HashMap<String, List<String>>();
                    // Check if the url was seen before
                    if (contentSeen(info.toString())) {

                        String lastChecked = this.db.getDateUrl(info.toString());
                        System.out.println("LastChecked:" + lastChecked);

                        headers = sendHeadReq(null, connection, true, lastChecked);

                        if (headers == null) {
                            logger.info(url + ": not modified");
                            return;
                        }

                        if (!checkHeaders(headers)) {
                            logger.info(url + ": not modified");
                            return;
                        }

                    } else {

                        headers = sendHeadReq(null, connection, true, "");

                        if (headers == null) {
                            logger.info(url + ": not modified");
                            return;
                        }

                        if (!checkHeaders(headers)) {
                            logger.info(url + ": not modified");
                            return;
                        }
                    }

                    if (waitCrawl(url)) {
                    }

                    String documentContents = getContent(urlVal, null, connection, false);
                    crawlerObj.incCount();
                    addURLsToQueue(info);

                    Date date = new Date();
                    SimpleDateFormat getDate = new SimpleDateFormat("EEE, dd-MMM-yy HH:mm:ss z");
                    this.db.addDocument(info.toString(), documentContents, getDate.format(date));
                    logger.info(url + ": downloading");
                    System.out.println(url + ": downloading");

                    // Add it to the map to check next time.
                    urlCrawledSet.add(url);
                    timeLast.put(url, getDate.format(date));
                }
            } else {
                logger.info(url + ": not modified");
            }
        } catch (ThreadInterruptedException t) {
            return;
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
