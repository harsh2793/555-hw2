package edu.upenn.cis.cis455.crawler;

import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class for threadpool.
 */
public class CrawlerThreadPool {

    public ArrayList<String> shareQueue = new ArrayList<String>();
    Logger logger = LogManager.getLogger(CrawlerThreadPool.class);
    Boolean flag = false;

    /**
     * Constructor for the threadpool class.
     */
    public CrawlerThreadPool() {

    }

    /**
     * Initialize the queue.
     * 
     * @param shareQueue
     */
    public void addQueue(ArrayList<String> shareQueue) {

        this.shareQueue = shareQueue;
    }

    /**
     * Notify all the other threads when stop comes and bring them out of wait.
     */
    public void stopQueue() {
        flag = true;

        synchronized (shareQueue) {

            shareQueue.notifyAll();
        }
    }

    /**
     * Enqueue the url.
     */
    public void urlEnqueue(String url) {

        // Add to task and then break.
        while (!flag) {

            synchronized (this.shareQueue) {

                // Create an object of HttpTask and set the socket and add it to the queue.
                shareQueue.add(url);

                // This would be logged in the log file created and to the console.
                logger.debug("Notifying after add");

                shareQueue.notifyAll();
                break;
            }
        }
    }

    /**
     * Dequeue the task.
     * 
     * @throws InterruptedException
     */
    public String urlDequeue() throws InterruptedException {

        String url = null;
        // Dequeue the task if the shared queue is not empty.
        while (!flag) {

            synchronized (shareQueue) {

                if (shareQueue.isEmpty()) {

                    try {

                        shareQueue.wait();

                    } catch (InterruptedException e) {

                        Thread.currentThread().interrupt();
                        return null;
                    }

                } else {

                    String result = shareQueue.remove(0);
                    // logger.debug("Notifying everyone we are removing an item");
                    shareQueue.notifyAll();
                    // logger.debug("Exiting queue with return");
                    return result;
                }
            }
        }

        return url;
    }
}
