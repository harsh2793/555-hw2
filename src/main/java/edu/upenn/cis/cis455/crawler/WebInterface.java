package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static spark.Spark.*;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegistrationHandler;
import edu.upenn.cis.cis455.crawler.handlers.SuccessLogin;
import edu.upenn.cis.cis455.crawler.handlers.LogoutUser;
import edu.upenn.cis.cis455.crawler.handlers.LookUpHandler;

public class WebInterface {
    public static void main(String args[]) {
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        port(45555);

        StorageInterface database = StorageFactory.getDatabaseInstance(args[0]);

        LoginFilter testIfLoggedIn = new LoginFilter(database);

        if (args.length == 2) {
            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }

        // Post test if Logged In.
        before("/*", "POST", testIfLoggedIn);

        // Implementing register.
        post("/register", new RegistrationHandler(database));

        // Implementing Logout Get and Post.
        post("/logout", new LogoutUser());
        get("/logout", new LogoutUser());

        // Handling index and success Login.
        post("/successLogin", new SuccessLogin());
        get("/successLogin", new SuccessLogin());
        post("/index.html", new SuccessLogin());

        // Lookup Completed.
        get("/lookup", new LookUpHandler(database));

        // Implementing Login.
        post("/login", new LoginHandler(database));

        post("/", new SuccessLogin());
        get("/", new SuccessLogin());

        awaitInitialization();
    }
}