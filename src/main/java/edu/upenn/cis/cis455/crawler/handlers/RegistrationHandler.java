package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.HaltException;
import spark.Request;
import spark.Response;
import spark.Route;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RegistrationHandler implements Route {

    StorageInterface database;

    /**
     * Constructor for the class.
     */
    public RegistrationHandler(StorageInterface database) {
        this.database = database;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        Logger logger = LogManager.getLogger(RegistrationHandler.class);
        String body = "";
        if (request.queryParams().contains("username") && request.queryParams().contains("password")) {

            logger.info("Registering a User successfully");
            System.out.println("Username " + request.queryParams("username"));
            System.out.println("Password " + request.queryParams("password"));

            // Hash the password.
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(request.queryParams("password").getBytes(StandardCharsets.UTF_8));
            String passEncoded = Base64.getEncoder().encodeToString(hash);

            // Add the entry to the DB.
            if (this.database.addUser(request.queryParams("username"), passEncoded) == -1) {

                // Return error in case username exists.
                response.status(409);
            } else {

                System.out.println("Successful register");
                body = "<!DOCTYPE html><html><head><title>Successful Registration</title></h1></head><body><a href=\"/login-form.html\">Registration Successfully completed</a></body></html>\r\n";
                response.status(200);
                response.type("text/html");
                response.header("Content-Length", String.valueOf(body.getBytes().length));
                System.out.println("User Added" + passEncoded);
            }

        } else {
            logger.info("Insufficient information provided");

            // TODO: Add halt error.
        }

        return body;
    }
}
