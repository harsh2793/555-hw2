package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import edu.upenn.cis.cis455.storage.StorageInterface;

public class LoginHandler implements Route {
    StorageInterface db;

    // Constructor.
    public LoginHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {

        // Get the username and password from query params.
        String user = req.queryParams("username");
        String pass = req.queryParams("password");

        // Encode password before sending to database.
        MessageDigest digest = null;
        try {

            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Hash for the password.
        byte[] hash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));
        String passEncoded = Base64.getEncoder().encodeToString(hash);

        System.err.println("Login request for " + user + " and " + pass);

        // Validate the login info.
        if (db.getSessionForUser(user, passEncoded)) {

            System.err.println("Logged in!");

            // Create a session
            Session session = req.session();

            // Set the attribute for the user: username and encoded password
            session.attribute("user", user);
            session.attribute("password", passEncoded);

            // Max timeout for 5 minutes.
            session.maxInactiveInterval(5 * 60);

            // Redirect to the successful login route.
            resp.redirect("/successLogin");

        } else {

            // Halt Error Incorrect credentials.
            resp.redirect("/login-form.html");
        }

        return "";
    }
}
