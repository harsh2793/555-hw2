package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Response;
import spark.Route;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogoutUser implements Route {

    Logger logger = LogManager.getLogger(LogoutUser.class);

    // Constructor.
    public LogoutUser() {
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        logger.info(request.queryParams("username") + ": User logging out");

        // Get the session
        request.session(false);

        // Invalidate the session
        request.session().invalidate();

        // Redirect to login // Response code 302
        response.redirect("/login-form.html", 302);

        return null;
    }

}
