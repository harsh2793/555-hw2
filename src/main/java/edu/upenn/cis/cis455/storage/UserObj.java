package edu.upenn.cis.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

/**
 * Class for the user details stored as an object.
 */
@Entity
public class UserObj {

    // Variables for user data.
    @PrimaryKey
    String username;

    String password;
    Integer userId;

    /**
     * Constructor for the user details.
     */
    UserObj() {
    }

    UserObj(String username, String password, Integer userId) {
        this.username = username;
        this.password = password;
        this.userId = userId;
        // Generate ID
    }

    // Get ID.

}
