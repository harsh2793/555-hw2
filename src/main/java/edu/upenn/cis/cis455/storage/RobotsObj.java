package edu.upenn.cis.cis455.storage;

import java.util.List;
import java.util.Map;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class RobotsObj {
    // Variables for user data.
    @PrimaryKey
    String nameVal;

    Map<String, List<String>> disallowed;
    Map<String, Integer> timeDelay;

    RobotsObj() {
    }

    RobotsObj(String nameVal, Map<String, List<String>> disallowed, Map<String, Integer> timeDelay) {

        this.nameVal = nameVal;
        this.disallowed = disallowed;
        this.timeDelay = timeDelay;
    }
}
