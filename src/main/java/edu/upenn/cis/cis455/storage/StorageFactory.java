package edu.upenn.cis.cis455.storage;

public class StorageFactory {
    public static StorageInterface getDatabaseInstance(String directory) {

        // Creating object for Storage Interface.
        StorageInterface newObj = new StorageImplement(directory);

        return newObj;
    }
}
