package edu.upenn.cis.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class UrlObj {

    // Variables for user data.
    @PrimaryKey
    String url;

    String docContents;
    int docId;
    String lastChecked;

    UrlObj() {

    }

    public UrlObj(String url, String docContents, int docId, String lastChecked) {

        this.url = url;
        this.docContents = docContents;
        this.docId = docId;
        this.lastChecked = lastChecked;
    }
}
