package edu.upenn.cis.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class DocObj {

    // Variables for user data.
    @PrimaryKey
    String docContents;

    // List of urls and doc Ids.
    String url;
    int docId;

    DocObj() {
    }

    DocObj(String docContents, String url, int docId) {

        this.docContents = docContents;
        this.url = url;
        this.docId = docId;
    }

}


/**
 * Hash(Doc Content) -> List URLs , doc ID, content, Date of Crawl? (Send in the head), isInvalid
 * New Hash -> .....
 * Host: Object[Set(disallowed) int CrawlDelay]
 * 
 * URL -> Hash(Doc Content)
 * 
 */
