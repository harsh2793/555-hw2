package edu.upenn.cis.cis455.storage;

public interface StorageInterface {

    /**
     * How many documents so far?
     */
    public int getCorpusSize();

    /**
     * Add a new document, getting its ID
     */
    public int addDocument(String url, String documentContents, String date);

    /**
     * Retrieves a document's contents by URL
     */
    public String getDocument(String url);

    /**
     * Adds a user and returns an ID
     */
    public int addUser(String username, String password);

    /**
     * Tries to log in the user, or else throws a HaltException
     */
    public boolean getSessionForUser(String username, String password);

    /**
     * Shuts down / flushes / closes the storage system
     */
    public void close();

    /**
     * Adding robots object.
     */
    public boolean addRobots(String nameVal, RobotDataObj robot);

    /**
     * Get robots.txt
     */
    public RobotDataObj getRobots(String nameVal);

    public String checkRobotExists(String nameVal);

    public boolean containsUrl(String url);

    public String getDateUrl(String url);
}
