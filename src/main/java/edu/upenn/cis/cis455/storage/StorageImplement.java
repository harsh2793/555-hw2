package edu.upenn.cis.cis455.storage;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.Transaction;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class StorageImplement implements StorageInterface {

    Environment env;
    EntityStore userVals;
    EntityStore DocsVals;
    EntityStore urlVals;
    EntityStore robotsVals;
    PrimaryIndex<String, UserObj> userStorage;
    PrimaryIndex<String, DocObj> DocStorage;
    PrimaryIndex<String, UrlObj> urlStorage;
    PrimaryIndex<String, RobotsObj> robotsStorage;
    int idUser = 0;
    int idDoc = 0;

    public StorageImplement(String directory) {

        // Berkeley DB API
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(true);
        env = new Environment(new File(directory), envConfig);

        StoreConfig storeConfig = new StoreConfig();
        storeConfig.setAllowCreate(true);
        storeConfig.setTransactional(true);

        userVals = new EntityStore(env, "UserVals", storeConfig);
        DocsVals = new EntityStore(env, "DocsVals", storeConfig);
        urlVals = new EntityStore(env, "urlVals", storeConfig);
        robotsVals = new EntityStore(env, "robotsVals", storeConfig);

        userStorage = userVals.getPrimaryIndex(String.class, UserObj.class);
        DocStorage = DocsVals.getPrimaryIndex(String.class, DocObj.class);
        urlStorage = urlVals.getPrimaryIndex(String.class, UrlObj.class);
        robotsStorage = robotsVals.getPrimaryIndex(String.class, RobotsObj.class);

        idUser = (int) userStorage.count();
        idDoc = (int) DocStorage.count();
    }

    public boolean docExists(String url, String documentContents) {
        // Hash the document content.
        MessageDigest digest = null;

        try {

            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        }

        // Hash the document content.
        byte[] hash = digest.digest(documentContents.getBytes(StandardCharsets.UTF_8));
        String docEncoded = Base64.getEncoder().encodeToString(hash);

        // Check if the storage contains username.
        if (DocStorage.contains(docEncoded)) {

            if (!urlStorage.contains(url)) {

                System.out.println("In the url database.");
            }

            return true;
        }

        return false;
    }

    public boolean addRobots(String nameVal, RobotDataObj robot) {

        // Check if the storage contains user name.
        if (robotsStorage.contains(nameVal)) {

            // System.out.println("Robot Exists.");

            return false;
        } else {

            Transaction txn = env.beginTransaction(null, null);

            RobotsObj newFile = new RobotsObj(nameVal, robot.disallow, robot.timeDelay);

            robotsStorage.put(txn, newFile);

            txn.commit();

            return true;
        }
    }

    public String checkRobotExists(String nameVal) {

        // Check if the url has an entry.
        if (robotsStorage.contains(nameVal)) {

            // System.out.println("Hostname is:" + nameVal);

            RobotsObj robo = robotsStorage.get(nameVal);

            return robo.nameVal;
        } else {

            return null;
        }

    }

    public RobotDataObj getRobots(String nameVal) {

        // Check if the url has an entry.
        if (robotsStorage.contains(nameVal)) {

            // System.out.println("Hostname is:" + nameVal);

            RobotsObj robo = robotsStorage.get(nameVal);

            // System.out.println("Got Val not null");

            RobotDataObj retDB = new RobotDataObj(robo.disallowed, robo.timeDelay);
            // retDB.printObj();

            return retDB;
        }

        // Return null if document mapping does not exist for the url.
        // System.out.println("Document does not exists");

        return null;
    }

    @Override
    public int getCorpusSize() {

        // Return size of corpus.
        return (int) DocStorage.count();
    }

    public String getDateUrl(String url) {

        if (urlStorage.contains(url)) {

            UrlObj doc = urlStorage.get(url);
            return doc.lastChecked;
        }
        return "";
    }

    @Override
    public int addDocument(String url, String documentContents, String date) {

        // Hash the document content.
        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Hash the document content.
        byte[] hash = digest.digest(documentContents.getBytes(StandardCharsets.UTF_8));
        String docEncoded = Base64.getEncoder().encodeToString(hash);

        // Check if the storage contains user name.
        if (DocStorage.contains(docEncoded)) {

            System.out.println("Document Exists");

            return -1;
        } else {

            System.out.println("Adding new document....");

            Transaction txn = env.beginTransaction(null, null);

            idDoc++;

            DocObj newDoc = new DocObj(docEncoded, url, idUser);
            DocStorage.put(txn, newDoc);

            txn.commit();
            System.out.println("Added:" + idDoc);

            txn = env.beginTransaction(null, null);
            UrlObj newUrl = new UrlObj(url, documentContents, idDoc, date);
            urlStorage.put(txn, newUrl);
            txn.commit();

            return idDoc;
        }
    }

    public boolean containsUrl(String url) {

        // Check if the url has an entry.
        if (urlStorage.contains(url)) {

            return true;
        }

        // Return null if document mapping does not exist for the url.
        System.out.println("Document does not exists");
        return false;
    }

    @Override
    public String getDocument(String url) {

        // Check if the url has an entry.
        if (urlStorage.contains(url)) {

            System.out.println("Username in getSession:" + url);

            UrlObj doc = urlStorage.get(url);

            return doc.docContents;
        }

        // Return null if document mapping does not exist for the url.
        System.out.println("Document does not exists");
        return null;
    }

    @Override
    public int addUser(String username, String password) {

        System.out.println("Username is: " + username);

        // Check if the storage contains username.
        if (userStorage.contains(username)) {

            System.out.println("Username Exists");

            return -1;
        } else {

            System.out.println("Adding new User....");

            Transaction txn = env.beginTransaction(null, null);

            idUser++;

            UserObj newUser = new UserObj(username, password, idUser);
            userStorage.put(txn, newUser);

            txn.commit();
            System.out.println("Added:" + idUser);

            return idUser;
        }
    }

    @Override
    public boolean getSessionForUser(String username, String password) {

        // Check if the user exists.
        if (userStorage.contains(username)) {

            System.out.println("Username in getSession:" + username);

            UserObj existUser = userStorage.get(username);
            // Check if the password matches for the user and return true if correct.
            if (existUser.password.equals(password)) {

                System.out.println("Username exists");
                return true;
            }
        }

        // Return false if user does not exist or password is incorrect.
        System.out.println("Username does not exists");
        return false;
    }

    @Override
    public void close() {

        // Discard the database handle.
        userVals.close();
        DocsVals.close();
        urlVals.close();
        robotsVals.close();
        env.close();
    }

}
