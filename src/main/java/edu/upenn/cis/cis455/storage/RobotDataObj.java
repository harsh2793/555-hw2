package edu.upenn.cis.cis455.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RobotDataObj {

    Map<String, List<String>> disallow = new HashMap<String, List<String>>();
    Map<String, Integer> timeDelay = new HashMap<String, Integer>();

    public RobotDataObj() {
    }

    public void printObj() {

        System.out.println("Starting with user agent");

        System.out.println("Printing disallow");

        for (String s : this.disallow.keySet()) {
            System.out.println(s + " " + this.disallow.get(s));
        }
        System.out.println("Printing delay");

        for (String s : this.timeDelay.keySet()) {
            System.out.println(s + " " + this.timeDelay.get(s));
        }
        System.out.println("Ending with user agent");
    }

    public void addEntry(String userAgent, List<String> disallowedContent, int timeDelay) {

        this.disallow.put(userAgent, disallowedContent);
        this.timeDelay.put(userAgent, timeDelay);
    }

    public RobotDataObj(Map<String, List<String>> disallow, Map<String, Integer> timeDelay) {

        this.disallow = disallow;
        this.timeDelay = timeDelay;
    }

    public List<String> getDisallowedLinks(String hostname) {

        if (disallow.containsKey(hostname)) {
            return disallow.get(hostname);
        }

        return null;
    }

    public Integer getCrawlDelay(String hostname) {

        if (timeDelay.containsKey(hostname)) {
            return timeDelay.get(hostname);
        }

        return -1;
    }

}
